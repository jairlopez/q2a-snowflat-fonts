# Q2A's SnowFlat theme fonts

This repository contains fonts that are incompatible with the GPL:

* [Font Awesome](https://fontawesome.com/v4.7.0/)
* [Ubuntu font](https://design.ubuntu.com/font/)
